<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ConverterController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Groupe de route pour l'authentification
 */
Route::group(['middleware' => ['auth']], function (){

    Route::get('/', [HomeController::class, 'index'])->name('home.index');


    /**
     * Groupe de route pour les conversion
     */
    Route::group(['prefix' => 'converter'], function (){

        Route::get('/', [ConverterController::class, 'index'])->name('converter.index');
        Route::post('/convert_pdf', [ConverterController::class, 'convert_pdf'])->name('converter.convert_pdf');

    });


});


Auth::routes();
