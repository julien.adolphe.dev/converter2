@extends('layout')


@section('content')


    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon">
                    <i class="flaticon2-chat-1 text-primary"></i>
                </span>
                <h3 class="card-label">Convertisseur CSV to PDF</h3>
            </div>
        </div>
        <form action="{{route('converter.convert_pdf')}}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="card-body">

                <div class="form-group">
                    <label for="">Fichier CSV à convetir</label>
                    <input type="file" name="file_csv" required class="form-control" accept=".csv">
                </div>

            </div>

            <div class="card-footer d-flex justify-content-between">
                <button type="submit" class="btn btn-success">Convertir</button>
            </div>

        </form>
    </div>



@endsection


@section('script')

    <script>
        toastr.success('message')
    </script>

@endsection
