<!DOCTYPE html>
<html lang="fr">
<!--begin::Head-->
<head>
    <meta charset="utf-8" />
    <title>Mon Panier | Tournée</title>
    <style>

        @page { margin: 30px; }
        body{
            margin: 0;
            font-family: 'Roboto', sans-serif;
        }

        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #3F4254;
            background-color: transparent;
        }
        table {
            border-collapse: collapse;
        }
        .table:not(.table-bordered) thead th, .table:not(.table-bordered) thead td {
            border-top: 0;
        }
        .table thead th, .table thead td {
            font-family: 'Roboto', sans-serif;
            font-weight: 700;
            font-size: 1rem;
            border-bottom-width: 1px;
            padding-top: 1rem;
            padding-bottom: 1rem;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #EBEDF3;
        }
        .table th, .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #EBEDF3;
        }
        .table th, .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #EBEDF3;
        }
        .table-bordered th, .table-bordered td {
            border: 1px solid #EBEDF3;
        }
        .table-sm th, .table-sm td {
            padding: 0.3rem !important;
        }

        tr.second th, tr.second td {
            background-color: #D1D1D1;
        }

        thead th {
            background-color: #0d8ddc;
            color: #ffffff;
        }

        table th {
            font-weight: 700;
        }

        @font-face {
            font-family: 'Roboto';
            font-style: normal;
            font-weight: 700;
            src: url('{{storage_path('fonts/Roboto-Bold.ttf')}}') format("truetype");
        }
        @font-face {
            font-family: 'Roboto';
            font-style: normal;
            font-weight: 500;
            src: url('{{storage_path('fonts/Roboto-Medium.ttf')}}') format("truetype");
        }
        @font-face {
            font-family: 'Roboto';
            font-style: normal;
            font-weight: 400;
            src: url('{{storage_path('fonts/Roboto-Regular.ttf')}}') format("truetype");
        }

    </style>
</head>
<!--end::Head-->
<!--begin::Body-->
<body>

<!--begin::Main-->
<main>


    <div style="width: 100%">

        <div style="width: 100%">

            <table class="table table-sm table-bordered" style="width: 100%">
                <thead>
                <tr>
                    <td colspan="{{sizeof($data[0])}}" style="border: none; text-align: center">
                        <div style="position: relative; height: 30px">
                            <div>
                                Conversion du {{date('d/m/Y')}}
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>

                    @for($i=0; $i < sizeof($data[0]); $i++)
                        <td>{{$data[0][$i]}}</td>
                    @endfor
                </tr>
                </thead>
                <tbody>
                @foreach($data as $key => $line)

                    @if($key > 0)
                    <tr>
                        @for($i=0; $i < sizeof($line); $i++)
                            <td>{{$line[$i]}}</td>
                        @endfor
                    </tr>
                    @endif

                @endforeach
                </tbody>
            </table>

        </div>

    </div>

</main>
<!--end::Main-->
</body>
<!--end::Body-->
</html>



<table>
    @foreach($data as $key => $line)

        <tr>
            @for($i=0; $i < sizeof($line); $i++)
                <td>{{$line[$i]}}</td>
            @endfor
        </tr>

    @endforeach
</table>

