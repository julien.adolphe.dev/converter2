<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ConverterController extends Controller
{

    /**
     * Fonction pour retourner la vue principale du convertisseur
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){

        //Session::flash('warning', 'Super');
        return view('converter.index');

    }

    /**
     * Fonction pour convertir un CSV en PDF
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function convert_pdf(Request $request)
    {

        // Récupère le fichier du formulaire
        $file = $request->file('file_csv');

        // Initialise le tableau qui recoit les info du CSV
        $tab_csv = [];

        // Cicrule chaque ligne du csv
        if (($handle = fopen($file->getPathname(), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

                // Pousse chaque ligne du csv au format tableau
                array_push($tab_csv, $data);

            }
            fclose($handle);
        }

        $pdf = PDF::loadView('pdf.csv_pdf', [
            'data' => $tab_csv
        ])->setPaper('a4', 'landscape');

        return $pdf->download('invoice.pdf');

    }


}
